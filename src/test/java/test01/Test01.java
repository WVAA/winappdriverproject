package test01;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.aspectj.util.FileUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.windows.WindowsDriver;

public class Test01 {
	
	public static WindowsDriver driver = null;
	
	@BeforeClass 
	public void setUp() {
		DesiredCapabilities cap = new  DesiredCapabilities();
		cap.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
		cap.setCapability("platformName", "Windows");
		cap.setCapability("deciveName", "WindowsPC");
		
		try {
		
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}
	
	
	@AfterSuite
	public void tearDown() {
		driver.quit();
	}
	
	@Test
	public void login() throws Exception {
		/*driver.findElement(By.name("Nueve")).click();
		driver.findElementByName("Multiplicar por").click();
		driver.findElementByName("Nueve").click();
		driver.findElementByName("Es igual a").click();
		driver.manage().timeouts().implicitlyWait(9, TimeUnit.SECONDS);
		driver.findElement(By.name("Cerrar Calculadora")).click();*/
		
		driver.findElementByAccessibilityId("num9Button").click();
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		File screenshotFile = new File("C:\\Users\\wa51898\\eclipse-workspace\\ProjectWin\\target\\capturas\\captura3.png");
		
		FileUtil.copyFile(scrFile, screenshotFile);

		
	}
	
	

}
