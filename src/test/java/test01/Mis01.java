package test01;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import io.appium.java_client.windows.WindowsDriver;

public class Mis01 {
	
	public static WindowsDriver driver = null;
	
	
	
	@BeforeClass 
	public void setUp() {
		DesiredCapabilities cap = new  DesiredCapabilities();
		cap.setCapability("app", "C:\\COBIS\\SV\\Mis\\mis_RTC14481.exe");
		cap.setCapability("platformName", "Windows");
		cap.setCapability("deviceName", "WindowsPC");
		
		try {
		
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	

	@AfterSuite
	public void tearDown() {
		driver.quit();
	}

	

	@Test
	public void loginMIS() throws Exception {
	Robot rb = new Robot();
	driver.findElementByName("Conexi�n").click();
	driver.findElementByName("Log On").click();
	Thread.sleep(10000);
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	rb.keyPress(KeyEvent.VK_TAB);
	
	rb.keyPress(KeyEvent.VK_B);
	rb.keyPress(KeyEvent.VK_C);
	rb.keyPress(KeyEvent.VK_O);
	rb.keyPress(KeyEvent.VK_B);
	rb.keyPress(KeyEvent.VK_I);
	rb.keyPress(KeyEvent.VK_S);
	rb.keyPress(KeyEvent.VK_0);
	rb.keyPress(KeyEvent.VK_2);
	rb.keyPress(KeyEvent.VK_D);
	rb.keyPress(KeyEvent.VK_A);
	
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	
	rb.keyPress(KeyEvent.VK_C);
	rb.keyPress(KeyEvent.VK_H);
	rb.keyPress(KeyEvent.VK_5);
	rb.keyPress(KeyEvent.VK_0);
	rb.keyPress(KeyEvent.VK_5);
	rb.keyPress(KeyEvent.VK_8);
	rb.keyPress(KeyEvent.VK_8);
	

	
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	
	rb.keyPress(KeyEvent.VK_SHIFT);
	rb.keyPress(KeyEvent.VK_C);
	rb.keyRelease(KeyEvent.VK_SHIFT);
	rb.keyPress(KeyEvent.VK_U);
	rb.keyPress(KeyEvent.VK_S);
	rb.keyPress(KeyEvent.VK_C);
	rb.keyPress(KeyEvent.VK_A);
	rb.keyPress(KeyEvent.VK_0);
	rb.keyPress(KeyEvent.VK_4);
	rb.keyPress(KeyEvent.VK_SHIFT);
	rb.keyPress(KeyEvent.VK_1);
	rb.keyRelease(KeyEvent.VK_SHIFT);
	Thread.sleep(5000);
	rb.keyPress(KeyEvent.VK_ALT);
	rb.keyPress(KeyEvent.VK_A);
	rb.keyRelease(KeyEvent.VK_ALT);
	
	

	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("Aceptar").click();
	
	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("Aceptar").click();
	
	
	/*
	Thread.sleep(5000);
	rb.keyPress(KeyEvent.VK_1);
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	rb.keyPress(KeyEvent.VK_1);
	rb.keyPress(KeyEvent.VK_TAB);
	rb.keyRelease(KeyEvent.VK_TAB);
	
	*/
	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("OK").click();
	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("Aceptar").click();
	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	//Buscando cliente
	driver.findElementByName("Clientes").click();
	driver.findElementByName("Personas").click();
	driver.findElementByName("Actualizar").click();
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("NIT:").click();
	driver.findElementByAccessibilityId("1").sendKeys("06142309831560");
	driver.findElementByName("Buscar").click();
	driver.findElementByName("Escoger").click();
	driver.findElementByName("Aceptar").click();
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByName("Aceptar").click();
	Thread.sleep(3000);	
	String dui = driver.findElementByAccessibilityId("20").getText();
	System.out.println("Dui de cliente "+dui);
	driver.findElementByName("Salir").click();
	driver.findElementByName("Clientes").click();
	driver.findElementByName("Consultas").click();
	driver.findElementByName("Datos Generales").click();
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	driver.findElementByAccessibilityId("2").sendKeys("77911995");
	driver.findElementByName("Generales").click();
	driver.findElementByName("Aceptar").click();
	driver.findElementByName("Aceptar").click();
	driver.findElementByName("Direcci�n").click();
	driver.findElementByName("Tel�fonos").click();
	Thread.sleep(3000);	
	driver.findElementByName("Salir").click();
	Thread.sleep(3000);
	driver.findElementByName("Conexi�n").click();
	driver.findElementByName("Log Off").click();
	Thread.sleep(3000);
	driver.findElementByName("Aceptar").click();
	Thread.sleep(3000);
	driver.findElement(By.name("Cerrar")).click();
	
	
	}

}
