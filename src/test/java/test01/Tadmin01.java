package test01;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.windows.WindowsDriver;

public class Tadmin01 {
	
	public static WindowsDriver driver = null;
	
		@BeforeClass 
		public void setUp() {
			DesiredCapabilities cap = new  DesiredCapabilities();
			cap.setCapability("app", "C:\\COBIS\\SV\\TADMINSV\\tadminsv.exe");
			cap.setCapability("platformName", "Windows");
			cap.setCapability("deviceName", "WindowsPC");
		
			try {
		
				driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
			}catch (MalformedURLException e) {
			e.printStackTrace();
			}
		
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		}

		
		@AfterSuite
		public void tearDown() {
			driver.quit();
		}
		
		
		
		@Test
		public void loginTadmin() throws Exception{
			Robot rb = new Robot();
			
			driver.findElementByName("Conexi�n").click();
			driver.findElementByName("Log on").click();
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			rb.keyPress(KeyEvent.VK_TAB);
			
			rb.keyPress(KeyEvent.VK_M);
			rb.keyPress(KeyEvent.VK_I);
			rb.keyPress(KeyEvent.VK_G);
			rb.keyPress(KeyEvent.VK_C);
			rb.keyPress(KeyEvent.VK_O);
			rb.keyPress(KeyEvent.VK_B);
			rb.keyPress(KeyEvent.VK_0);
			rb.keyPress(KeyEvent.VK_1);
			rb.keyPress(KeyEvent.VK_L);
			rb.keyPress(KeyEvent.VK_A);
			
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			
			rb.keyPress(KeyEvent.VK_M);
			rb.keyPress(KeyEvent.VK_R);
			rb.keyPress(KeyEvent.VK_5);
			rb.keyPress(KeyEvent.VK_1);
			rb.keyPress(KeyEvent.VK_7);
			rb.keyPress(KeyEvent.VK_3);
			rb.keyPress(KeyEvent.VK_3);
			
			
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			
			rb.keyPress(KeyEvent.VK_SHIFT);
			rb.keyPress(KeyEvent.VK_N);
			rb.keyRelease(KeyEvent.VK_SHIFT);
			rb.keyPress(KeyEvent.VK_A);
			rb.keyPress(KeyEvent.VK_V);
			rb.keyPress(KeyEvent.VK_I);
			rb.keyPress(KeyEvent.VK_D);
			rb.keyPress(KeyEvent.VK_A);
			rb.keyPress(KeyEvent.VK_D);
			rb.keyPress(KeyEvent.VK_2);
			rb.keyPress(KeyEvent.VK_0);
			rb.keyPress(KeyEvent.VK_2);
			rb.keyPress(KeyEvent.VK_0);
			
			rb.keyPress(KeyEvent.VK_ALT);
			rb.keyPress(KeyEvent.VK_A);
			rb.keyRelease(KeyEvent.VK_ALT);
			
			
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementByName("Aceptar").click();
			
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementByName("Aceptar").click();
			
			
			Thread.sleep(5000);
			rb.keyPress(KeyEvent.VK_1);
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			rb.keyPress(KeyEvent.VK_1);
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			
			
				
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementByName("OK").click();
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.findElementByName("Aceptar").click();
			
			
			
			Thread.sleep(60000);
			
		}
		

		
		
	}
