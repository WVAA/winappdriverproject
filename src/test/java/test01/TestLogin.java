package test01;

import static org.testng.Assert.assertEquals;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.windows.WindowsDriver;
import pantallas.LogOut;
import pantallas.Login;
import tools.ScreenShot;

public class TestLogin {
	
	WindowsDriver driver = null;
	
	
	
	@BeforeClass 
	public void setUp() {
		DesiredCapabilities cap = new  DesiredCapabilities();
		cap.setCapability("app", "C:\\COBIS\\SV\\Mis\\mis_RTC14481.exe");
		cap.setCapability("platformName", "Windows");
		cap.setCapability("deviceName", "WindowsPC");
		
		try {
		
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@AfterSuite
	public void tearDown() throws Exception {
		LogOut objLogOut = new LogOut(driver);
		objLogOut.salirSesion();
		driver.quit();
	}
	
	
	@Test(testName = "Login",description = "Inicio de sesion en modulo Clientes")
	public void loginMIS() throws Exception {
		
		Login objLogin = new Login(driver);
		ScreenShot captura = new ScreenShot(driver);
		
		objLogin.inicioSesionMis();
		assertEquals(driver.findElement(By.name("ACTUALIZACION")).getText(),"ACTUALIZACION");
		captura.tomarCaptura("Login");
		Thread.sleep(5000);
		
	}

}
