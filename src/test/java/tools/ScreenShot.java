package tools;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.aspectj.util.FileUtil;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.windows.WindowsDriver;

public class ScreenShot {
	
	WindowsDriver driver = null;
	
	public ScreenShot(WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	public void tomarCaptura(String nombreCaptura) throws IOException {
	
	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	
	File screenshotFile = new File("C:\\Users\\wa51898\\eclipse-workspace\\Cobis02\\target\\capturas\\"+nombreCaptura+".png");
	
	FileUtil.copyFile(scrFile, screenshotFile);
	
	}

}
