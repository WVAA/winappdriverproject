package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.windows.WindowsDriver;
import pantallas.Apertura;
import pantallas.CreacionRapida;
import pantallas.LogOut;
import pantallas.Login;

public class Personas {
	
	WindowsDriver driver = null;
	
	
	
	@BeforeClass 
	public void setUp() {
		DesiredCapabilities cap = new  DesiredCapabilities();
		cap.setCapability("app", "C:\\COBIS\\SV\\Mis\\mis_RTC14481.exe");
		cap.setCapability("platformName", "Windows");
		cap.setCapability("deviceName", "WindowsPC");
		
		try {
		
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@AfterSuite
	public void tearDown() throws Exception {
		LogOut objLogOut = new LogOut(driver);
		objLogOut.salirSesion();
		driver.quit();
	}
	
	@Test(testName = "AperturaPersona",description = "Apertura de cliente persona natural", enabled = false)
	public void aperturaPersona() throws Exception {
		Login objLogin = new Login(driver);
		Apertura objApertura = new Apertura(driver);
		
		objLogin.inicioSesionMis();
		System.out.println("Inicia Sesion");
		objApertura.aperturaCliente("F", "Maritza Concepcion", "Gomez", "Lara", "999990063", "0106", "1500", "N");
		System.out.println("Crea el cliente correctamente");
		//pendiente definicion de assert
	}
	
	@Test(testName = "CreacionRapidaPersona", description="Creacion rapida de cliente persona natural", enabled = true )
	public void creacionRapidaPersona() throws Exception {
		Login objLogin = new Login(driver);
		CreacionRapida objCreaRap = new CreacionRapida(driver);
		objLogin.inicioSesionMis();
		System.out.println("Inicia Sesion");
		objCreaRap.creacionRapidaPersona("999990071", "Blanca Vilma", "Rivera", "Sanchez", "F", "S", "0141", "1", "1", "101", "CtaAho");
		System.out.println("Crea el cliente correctamente");
				
	}

}
