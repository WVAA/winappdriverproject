package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.windows.WindowsDriver;
import pantallas.AperturaEmpresa;
import pantallas.CreacionRapEmpresa;
import pantallas.LogOut;
import pantallas.Login;

public class Empresas {
	
WindowsDriver driver = null;
	
	
	
	@BeforeClass 
	public void setUp() {
		DesiredCapabilities cap = new  DesiredCapabilities();
		cap.setCapability("app", "C:\\COBIS\\SV\\Mis\\mis_RTC14481.exe");
		cap.setCapability("platformName", "Windows");
		cap.setCapability("deviceName", "WindowsPC");
		
		try {
		
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"),cap);
			
		}catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	@AfterSuite
	public void tearDown() throws Exception {
		LogOut objLogOut = new LogOut(driver);
		objLogOut.salirSesion();
		driver.quit();
	}
	
	
	@Test(description = "Apertura de Empresa", enabled = false)
	public void aperturaEmpresa() throws Exception {
		
		Login objLogin = new Login(driver);
		AperturaEmpresa objAperturaEmp = new AperturaEmpresa(driver);
		
		objLogin.inicioSesionMis();
		System.out.println("Inicia Sesion");
		objAperturaEmp.AperturaCompany("Cornwood", "100", "3", "MB", "74490840", "", "120", "10101");
		//definir assert
	}
	
	@Test(description = "Creacion Rapida de empresa")
	public void creacionRapidaEmp() throws Exception {
		Login objLogin = new Login(driver);
		CreacionRapEmpresa objCreRapEmp = new CreacionRapEmpresa(driver);
		
		objLogin.inicioSesionMis();
		System.out.println("Inicia Sesion");
		objCreRapEmp.creacionRapidaEmpresa("La Caixa", "3", "74119026", "1", "120", "1", "1", "101", "PE");
		System.out.println("Creacion de empresa");
		//definir assert
	}
	

}
