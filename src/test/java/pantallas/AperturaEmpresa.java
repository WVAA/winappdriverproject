package pantallas;

import org.openqa.selenium.By;

import io.appium.java_client.MobileBy.ByAccessibilityId;
import io.appium.java_client.windows.WindowsDriver;

public class AperturaEmpresa {
	
	WindowsDriver driver = null;
	
	By menuClientes = By.name("Clientes");
	By opcionCompany = By.name("Compa��as");
	By opcionApertura = By.name("Apertura");
	
	//botones radio
	By rdOIDSi = By.name("Si");
	By rdOIDNo = By.name("No");
	By rdRetencionSi = By.name("Si");
	By rdRetencionNo = By.name("No");
	
	By txtCompany = ByAccessibilityId.AccessibilityId("11");
	//TAB para NIT
	By txtActEconomica = ByAccessibilityId.AccessibilityId("13");
	By txtSectEconomico = ByAccessibilityId.AccessibilityId("14");
	By txtNacionalidad = ByAccessibilityId.AccessibilityId("17");
	By txtCalificCliente = ByAccessibilityId.AccessibilityId("12"); 
	By txtRepresentanteLegal = ByAccessibilityId.AccessibilityId("16");
	By txtGrupoEconomico = ByAccessibilityId.AccessibilityId("15");
	By txtRegFiscal = ByAccessibilityId.AccessibilityId("9");
	By txtIva = ByAccessibilityId.AccessibilityId("6");
	By txtSecInsti = ByAccessibilityId.AccessibilityId("4");
	By txtGiro = ByAccessibilityId.AccessibilityId("8");
	By txtAreaComentarios = ByAccessibilityId.AccessibilityId("10");
	
	//Guardar
	By btnTransmitir = By.name("Transmitir");
	
	
	public AperturaEmpresa(WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	public void clickClientes() {
		driver.findElement(menuClientes).click();
	}
	
	public void clickCompany() {
		driver.findElement(opcionCompany).click();
	}
	
	public void clickApertura() {
		driver.findElement(opcionApertura).click();
	}
	
	public void clickTransmitir() {
		driver.findElement(btnTransmitir).click();
	}


	public void setTxtCompany(String strCompany) {
		driver.findElement(txtCompany).sendKeys(strCompany);
	}


	public void setTxtActEconomica(String strActEconomica) {
		driver.findElement(txtActEconomica).sendKeys(strActEconomica);
	}


	public void setTxtSectEconomico(String strSectEconomico) {
		driver.findElement(txtSectEconomico).sendKeys(strSectEconomico);
	}


	public void setTxtNacionalidad(String strNacionalidad) {
		driver.findElement(txtNacionalidad).sendKeys(strNacionalidad);
	}


	public void setTxtCalificCliente(String strCalificCliente) {
		driver.findElement(txtCalificCliente).sendKeys(strCalificCliente);
	}


	public void setTxtRepresentanteLegal(String strRepresentanteLegal) {
		driver.findElement(txtRepresentanteLegal).sendKeys(strRepresentanteLegal);
	}


	public void setTxtGrupoEconomico(String strGrupoEconomico) {
		driver.findElement(txtGrupoEconomico).sendKeys(strGrupoEconomico);
	}


	public void setTxtRegFiscal(String strRegFiscal) {
		driver.findElement(txtRegFiscal).sendKeys(strRegFiscal);
	}


	public void setTxtIva(String strIva) {
		driver.findElement(txtIva).sendKeys(strIva);
	}


	public void setTxtSecInsti(String strSecInsti) {
		driver.findElement(txtSecInsti).sendKeys(strSecInsti);
	}


	public void setTxtGiro(String strGiro) {
		driver.findElement(txtGiro).sendKeys(strGiro);
	}


	public void setTxtAreaComentarios(String strAreaComentarios) {
		driver.findElement(txtAreaComentarios).sendKeys(strAreaComentarios);
	}
	
	
	//metodos
	
	public void AperturaCompany(String nombreC, String actividadEc, String secEco, String califiCli, String RepLegal, String GruEco,
			String secInst, String giroEc) {
		
		this.clickClientes();
		this.clickCompany();
		this.clickApertura();
		
		this.setTxtCompany(nombreC);
		this.setTxtActEconomica(actividadEc);
		this.setTxtSectEconomico(secEco);
		this.setTxtCalificCliente(califiCli);
		this.setTxtRepresentanteLegal(RepLegal);
		this.setTxtGrupoEconomico(GruEco);
		this.setTxtSecInsti(secInst);
		this.setTxtGiro(giroEc);
		
		
		
		this.clickTransmitir();
		
	}
	
	
	
}
