package pantallas;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;

import io.appium.java_client.MobileBy.ByAccessibilityId;
import io.appium.java_client.windows.WindowsDriver;

public class CreacionRapEmpresa {
	
	WindowsDriver driver = null;
	
	By menuClientes = By.name("Clientes");
	By opcionCompany = By.name("Compa��as");
	By opcionApertura = By.name("Creaci�n Rapida");
	
	By txtCompany = ByAccessibilityId.AccessibilityId("14");
	//TAB para NIT
	By txtSectEconomico = ByAccessibilityId.AccessibilityId("12");
	By txtRepresentanteLegal = ByAccessibilityId.AccessibilityId("9");
	By txtNacionalidad = ByAccessibilityId.AccessibilityId("8");
	By txtSecInsti = ByAccessibilityId.AccessibilityId("7");
	By txtPais = ByAccessibilityId.AccessibilityId("4");
	By txtDepto = ByAccessibilityId.AccessibilityId("6");
	By txtCiudad = ByAccessibilityId.AccessibilityId("5");
	By txtTamEmp = ByAccessibilityId.AccessibilityId("3");
	By txtAreaComent = ByAccessibilityId.AccessibilityId("13");
	
	//Guardar
	By btnTransmitir = By.name("Transmitir");
	
	
	public CreacionRapEmpresa(WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	public void ingresoNIT(String nit) {
		/*Robot rb = new Robot();
		String sDig;
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		//Thread.sleep(2000);
		
		/*for(int i=0; i<=(nit.length())-1; i++) {
			
			sDig = nit.substring(i, i+1);
		
			/*switch(sDig) {
				
			case "0": rb.keyPress(KeyEvent.VK_0);
						break;
			case "1": rb.keyPress(KeyEvent.VK_1);
						break;
			case "2": rb.keyPress(KeyEvent.VK_2);
						break;
			case "3": rb.keyPress(KeyEvent.VK_3);
						break;
			case "4": rb.keyPress(KeyEvent.VK_4);
						break;
			case "5": rb.keyPress(KeyEvent.VK_5);
						break;
			case "6": rb.keyPress(KeyEvent.VK_6);
						break;
			case "7": rb.keyPress(KeyEvent.VK_7);
						break;
			case "8": rb.keyPress(KeyEvent.VK_8);
						break;
			case "9": rb.keyPress(KeyEvent.VK_9);
						break;
			default :  System.out.print("Error");
						break;
			}*/
	}
	
	
	public void creacionRapidaEmpresa(String nombreC, String sectorEc, String repLegal, String nacionalidad, String secInst,
			String pais, String depto, String municipio, String tamaEmp) throws Exception {
		
		driver.findElement(menuClientes).click();
		driver.findElement(opcionCompany).click();
		driver.findElement(opcionApertura).click();
		
		driver.findElement(txtCompany).sendKeys(nombreC);
		//tab para NIT
		driver.findElement(txtSectEconomico).sendKeys(sectorEc);
		driver.findElement(txtRepresentanteLegal).sendKeys(repLegal);
		driver.findElement(txtNacionalidad).sendKeys(nacionalidad);
		driver.findElement(txtSecInsti).sendKeys(secInst);
		driver.findElement(txtPais).sendKeys(pais);
		driver.findElement(txtDepto).sendKeys(depto);
		driver.findElement(txtCiudad).sendKeys(municipio);
		
		driver.findElement(txtTamEmp).sendKeys(tamaEmp);
		Thread.sleep(5000);
		driver.findElement(btnTransmitir).click();
		
	}
}
