package pantallas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;

import io.appium.java_client.MobileBy.ByAccessibilityId;
import io.appium.java_client.windows.WindowsDriver;

public class Apertura {
	
	WindowsDriver driver = null;
	
	By menuClientes = By.name("Clientes");
	By opcionPersonas = By.name("Personas");
	By opcionApertura = By.name("Apertura");
	
	//Datos generales apertura
	
	By txtSexo = ByAccessibilityId.AccessibilityId("19");
	By txtNombres = ByAccessibilityId.AccessibilityId("18");
	By txtPrimerApellido = ByAccessibilityId.AccessibilityId("34");
	By txtSegundoApellido = ByAccessibilityId.AccessibilityId("33");
	//doble TAB para fecha de nacimiento
	By txtNacionalidad = ByAccessibilityId.AccessibilityId("30");
	By txtDui = ByAccessibilityId.AccessibilityId("20");
	//TAB para fecha de expedicion de dui
	//doble TAB para nit
	//para seleccionar opcion de sujeto a retencion ubicarse en partida defunsion y TAB		
	By txtCarnetResidente = ByAccessibilityId.AccessibilityId("21");
	By txtPasaporte = ByAccessibilityId.AccessibilityId("32");
	By txtIva = ByAccessibilityId.AccessibilityId("23");
	By txtProfesion = ByAccessibilityId.AccessibilityId("29");
	By txtActividadEconomica = ByAccessibilityId.AccessibilityId("25");
	By txtEstadoCivil = ByAccessibilityId.AccessibilityId("31");
	By txtClasificacionCliente = ByAccessibilityId.AccessibilityId("28");
	By txtAreaComentarios = ByAccessibilityId.AccessibilityId("26");
	
	//Datos Adicionales
	By btnDatosAdic = By.name("Datos Adic.");
	By txtCarnetMinoridad = ByAccessibilityId.AccessibilityId("6");
	By txtLicenciaConducir = ByAccessibilityId.AccessibilityId("14");
	By txtRegistroFiscal = ByAccessibilityId.AccessibilityId("13");
	By txtGiro = ByAccessibilityId.AccessibilityId("12");//validar como completar
	By txtCalifCliente = ByAccessibilityId.AccessibilityId("11");
	By txtDomiciliado = ByAccessibilityId.AccessibilityId("7");
	By txtDependientes = ByAccessibilityId.AccessibilityId("8");
	By txtTipobanca = ByAccessibilityId.AccessibilityId("3");
	By txtTamEmpresa = ByAccessibilityId.AccessibilityId("2");
	By txtAreaComAdicionales = ByAccessibilityId.AccessibilityId("9");
	By txtAreaComent = ByAccessibilityId.AccessibilityId("4");
	
	//Guardar
	By btnTransmitir = By.name("Transmitir");
	
		
	
	public Apertura (WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	//metodos setter
	
	public void clickClientes() {
		driver.findElement(menuClientes).click();
	}
	
	public void clickPersonas() {
		driver.findElement(opcionPersonas).click();
	}
	
	public void clickApertura() {
		driver.findElement(opcionApertura).click();
	}
	
	public void clickTransmitir() {
		driver.findElement(btnTransmitir).click();
	}
	
	public void clickDatosAdic() {
		driver.findElement(btnDatosAdic).click();
	}


	public void setTxtSexo(String strSexo) {
		driver.findElement(txtSexo).sendKeys(strSexo);
	}


	public void setTxtNombres(String strNombres) {
		driver.findElement(txtNombres).sendKeys(strNombres);
	}


	public void setTxtPrimerApellido(String strPrimerApellido) {
		driver.findElement(txtPrimerApellido).sendKeys(strPrimerApellido);
	}


	public void setTxtSegundoApellido(String strSegundoApellido) {
		driver.findElement(txtSegundoApellido).sendKeys(strSegundoApellido);
	}


	public void setTxtNacionalidad(String strNacionalidad) {
		driver.findElement(txtNacionalidad).sendKeys(strNacionalidad);
	}


	public void setTxtDui(String strDui) {
		driver.findElement(txtDui).sendKeys(strDui);
	}


	public void setTxtCarnetResidente(String strCarnetResidente) {
		driver.findElement(txtCarnetResidente).sendKeys(strCarnetResidente);
	}


	public void setTxtPasaporte(String strPasaporte) {
		driver.findElement(txtPasaporte).sendKeys(strPasaporte);
	}


	public void setTxtIva(String strIva) {
		driver.findElement(txtIva).sendKeys(strIva);
	}


	public void setTxtProfesion(String strProfesion) {
		driver.findElement(txtProfesion).sendKeys(strProfesion);
	}


	public void setTxtActividadEconomica(String strActividadEconomica) {
		driver.findElement(txtActividadEconomica).sendKeys(strActividadEconomica);
	}


	public void setTxtEstadoCivil(String strEstadoCivil) {
		driver.findElement(txtEstadoCivil).sendKeys(strEstadoCivil);
	}


	public void setTxtClasificacionCliente(String strClasificacionCliente) {
		driver.findElement(txtClasificacionCliente).sendKeys(strClasificacionCliente);
	}


	public void setTxtAreaComentarios(String strAreaComentarios) {
		driver.findElement(txtAreaComentarios).sendKeys(strAreaComentarios);
	}


	public void setTxtCarnetMinoridad(String strCarnetMinoridad) {
		driver.findElement(txtCarnetMinoridad).sendKeys(strCarnetMinoridad);
	}


	public void setTxtLicenciaConducir(String strLicenciaConducir) {
		driver.findElement(txtLicenciaConducir).sendKeys(strLicenciaConducir);
	}


	public void setTxtRegistroFiscal(String strRegistroFiscal) {
		driver.findElement(txtRegistroFiscal).sendKeys(strRegistroFiscal);
	}


	public void setTxtGiro(String strGiro) {
		driver.findElement(txtGiro).sendKeys(strGiro);
	}


	public void setTxtCalifCliente(String strCalifCliente) {
		driver.findElement(txtCalifCliente).sendKeys(strCalifCliente);
	}


	public void setTxtDomiciliado(String strDomiciliado) {
		driver.findElement(txtDomiciliado).sendKeys(strDomiciliado);
	}


	public void setTxtDependientes(String strDependientes) {
		driver.findElement(txtDependientes).sendKeys(strDependientes);
	}


	public void setTxtTipobanca(String strTipobanca) {
		driver.findElement(txtTipobanca).sendKeys(strTipobanca);
	}


	public void setTxtTamEmpresa(String strTamEmpresa) {
		driver.findElement(txtTamEmpresa).sendKeys(strTamEmpresa);
	}


	public void setTxtAreaComAdicionales(String strAreaComAdicionales) {
		driver.findElement(txtAreaComAdicionales).sendKeys(strAreaComAdicionales);
	}


	public void setTxtAreaComent(String strAreaComent) {
		driver.findElement(txtAreaComent).sendKeys(strAreaComent);
	}
	
	
	//metodos para aperturar cliente
	
	public void datosAdicionales(String califC) throws Exception {
		
		Robot rb = new Robot();
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		Thread.sleep(2000);
		driver.findElementByAccessibilityId("2").click();
		driver.findElement(txtGiro).click();
		rb.keyPress(KeyEvent.VK_F5);
		rb.keyRelease(KeyEvent.VK_F5);
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		this.setTxtCalifCliente(califC);
		
		this.clickTransmitir();
		
		
	}
	
	public void fechaDUI() throws Exception {
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		Thread.sleep(2000);
		rb.keyPress(KeyEvent.VK_2);
		rb.keyPress(KeyEvent.VK_3);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_9);
		rb.keyPress(KeyEvent.VK_2);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_1);
		rb.keyPress(KeyEvent.VK_8);
	}
	
	public void fechaNacimiento() throws Exception {
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		Thread.sleep(2000);
		rb.keyPress(KeyEvent.VK_2);
		rb.keyPress(KeyEvent.VK_3);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_9);
		rb.keyPress(KeyEvent.VK_1);
		rb.keyPress(KeyEvent.VK_9);
		rb.keyPress(KeyEvent.VK_8);
		rb.keyPress(KeyEvent.VK_3);
	}
	
	
	public void aperturaCliente(String sexo,String nombres, String primerApell, String segundoApell, String dui,
			String profesion, String actividadEco, String califC) throws Exception {
		
		this.clickClientes();
		this.clickPersonas();
		this.clickApertura();
		
		
		this.setTxtSexo(sexo);
		this.setTxtNombres(nombres);
		this.setTxtPrimerApellido(primerApell);
		this.setTxtSegundoApellido(segundoApell);
		
		//rutina para fecha de nacimiento
		this.fechaNacimiento();
		
		this.setTxtDui(dui);
		
		//rutina para fecha emision dui
		this.fechaDUI();
		
		
		//rutina para NIT
		
		this.setTxtProfesion(profesion);
		this.setTxtActividadEconomica(actividadEco);
		
		
		Thread.sleep(2000);
		
		this.datosAdicionales(califC);
		
			
		Thread.sleep(10000);
		this.clickTransmitir();
		
		//agregar clic en aceptar cuadro dialogo
		
	}
	
	
	

}
