package pantallas;

import org.openqa.selenium.By;

import io.appium.java_client.windows.WindowsDriver;

public class LogOut {
	
WindowsDriver driver = null;
	
	By menuConexion = By.name("Conexi�n");
	By opcionLogOff = By.name("Log Off");
	
	public LogOut(WindowsDriver driver) {
		this.driver=driver;
	}
	
	public void clicConexion() {
		driver.findElement(menuConexion).click();;
	}
	
	public void clicLogOff() {
		driver.findElement(opcionLogOff).click();;
	}
	
	public void salirSesion() throws Exception {
		
		this.clicConexion();
		this.clicLogOff();
		
		Thread.sleep(3000);
		driver.findElementByName("Aceptar").click();
		Thread.sleep(3000);
		driver.findElement(By.name("Cerrar")).click();
	}

}
