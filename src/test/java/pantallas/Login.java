package pantallas;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import io.appium.java_client.windows.WindowsDriver;

public class Login {
	
	WindowsDriver driver = null;
	
	By menuConexion = By.name("Conexi�n");
	By opcionLogOn = By.name("Log On");
	
	
	public Login(WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	public void clicConexion() {
		driver.findElement(menuConexion).click();
	}
	
	public void clicLogOn() {
		driver.findElement(opcionLogOn).click();
	}
	
	
	public void inicioSesionMis() throws Exception {
		Robot rb = new Robot();
		
			
		this.clicConexion();
		this.clicLogOn();
		
		Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		rb.keyPress(KeyEvent.VK_TAB);
		
		rb.keyPress(KeyEvent.VK_B);
		rb.keyPress(KeyEvent.VK_C);
		rb.keyPress(KeyEvent.VK_O);
		rb.keyPress(KeyEvent.VK_B);
		rb.keyPress(KeyEvent.VK_I);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_2);
		rb.keyPress(KeyEvent.VK_D);
		rb.keyPress(KeyEvent.VK_A);
		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		
		rb.keyPress(KeyEvent.VK_C);
		rb.keyPress(KeyEvent.VK_H);
		rb.keyPress(KeyEvent.VK_5);
		rb.keyPress(KeyEvent.VK_0);
		rb.keyPress(KeyEvent.VK_5);
		rb.keyPress(KeyEvent.VK_8);
		rb.keyPress(KeyEvent.VK_8);
		

		
		rb.keyPress(KeyEvent.VK_TAB);
		rb.keyRelease(KeyEvent.VK_TAB);
		
		rb.keyPress(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_C);
		rb.keyRelease(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_U);
		rb.keyPress(KeyEvent.VK_S);
		rb.keyPress(KeyEvent.VK_C);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyPress(KeyEvent.VK_4);
		rb.keyPress(KeyEvent.VK_SHIFT);
		rb.keyPress(KeyEvent.VK_1);
		rb.keyRelease(KeyEvent.VK_SHIFT);
		Thread.sleep(10000);
		rb.keyPress(KeyEvent.VK_ALT);
		rb.keyPress(KeyEvent.VK_A);
		rb.keyRelease(KeyEvent.VK_ALT);
		
		

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByName("Aceptar").click();
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByName("Aceptar").click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByName("OK").click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByName("Aceptar").click();
		
		
	}
	

}
