package pantallas;

import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;

import io.appium.java_client.MobileBy.ByAccessibilityId;
import io.appium.java_client.windows.WindowsDriver;

public class CreacionRapida {
	
	WindowsDriver driver = null;
	
	
	By menuClientes = By.name("Clientes");
	By opcionPersonas = By.name("Personas");
	By opcionApertura = By.name("Creaci�n R�pida");
	
	By rdExtranjero = By.name("Extranjeros");
	By rdMenor = By.name("Menor de Edad");
	By rdAhoPla = By.name("Ahorros/Plazo");
	By rdCte = By.name("Cta. Cte.");
	By txtDui = ByAccessibilityId.AccessibilityId("12");
	//TAB para agregar NIT
	By txtSexo = ByAccessibilityId.AccessibilityId("16");
	By txtNombres = ByAccessibilityId.AccessibilityId("17");
	By txtPrimerApellido = ByAccessibilityId.AccessibilityId("18");
	By txtSegundoApellido = ByAccessibilityId.AccessibilityId("19");
	By txtEstadoCivil = ByAccessibilityId.AccessibilityId("14");
	//para fecha de nacimiento dos TAB despues de estado civil
	By txtProfesion = ByAccessibilityId.AccessibilityId("5");
	By txtPais = ByAccessibilityId.AccessibilityId("2");
	By txtDepto = ByAccessibilityId.AccessibilityId("4");
	By txtCiudad = ByAccessibilityId.AccessibilityId("3");
	
	//si es extranjero
	By txtPasaporte = ByAccessibilityId.AccessibilityId("10");
	//TAB para fecha de expedicion
	//TAB para fecfa de vencimiento
	By txtNacionalidad = ByAccessibilityId.AccessibilityId("9");
	By txtCarnetRes = ByAccessibilityId.AccessibilityId("11");
	
	//si es menor de edad
	By txtCarnetMinoridad = ByAccessibilityId.AccessibilityId("8");
	By txtPartidaNacimiento = ByAccessibilityId.AccessibilityId("7");
	
	//Guardar
	By btnTransmitir = By.name("Transmitir");
	
	
	public CreacionRapida(WindowsDriver driver) {
		this.driver = driver;
	}
	
	
	//Setters
	
	public void clickClientes() {
		driver.findElement(menuClientes).click();
	}
	
	public void clickPersonas() {
		driver.findElement(opcionPersonas).click();
	}
	
	public void clickCreacionRapida() {
		driver.findElement(opcionApertura).click();
	}
	
	public void clickTransmitir() {
		driver.findElement(btnTransmitir).click();
	}
	
	public void clicExtranjero() {
		driver.findElement(rdExtranjero).click();
	}
	
	public void clickMenor() {
		driver.findElement(rdMenor).click();
	}
	
	public void clickCtaCte() {
		driver.findElement(rdCte).click();
	}
	
	public void clickAho() {
		driver.findElement(rdAhoPla).click();
	}
	
	
	public void setTxtSexo(String strSexo) {
		driver.findElement(txtSexo).sendKeys(strSexo);
	}


	public void setTxtNombres(String strNombres) {
		driver.findElement(txtNombres).sendKeys(strNombres);
	}


	public void setTxtPrimerApellido(String strPrimerApellido) {
		driver.findElement(txtPrimerApellido).sendKeys(strPrimerApellido);
	}


	public void setTxtSegundoApellido(String strSegundoApellido) {
		driver.findElement(txtSegundoApellido).sendKeys(strSegundoApellido);
	}
	
	public void setTxtDui(String strDui) {
		driver.findElement(txtDui).sendKeys(strDui);
	}

	
	public void setTxtProfesion(String strProfesion) {
		driver.findElement(txtProfesion).sendKeys(strProfesion);
	}
	
	public void setTxtEstadoCivil(String strEstadoCivil) {
		driver.findElement(txtEstadoCivil).sendKeys(strEstadoCivil);
	}


	public void setTxtPais(String strPais) {
		driver.findElement(txtPais).sendKeys(strPais);
	}


	public void setTxtDepto(String strDepto) {
		driver.findElement(txtDepto).sendKeys(strDepto);
	}


	public void setTxtCiudad(String strCiudad) {
		driver.findElement(txtCiudad).sendKeys(strCiudad);
	}
	
		
	public void setTxtPasaporte(String strPasaporte) {
		driver.findElement(txtPasaporte).sendKeys(strPasaporte);
	}


	public void setTxtNacionalidad(String strNacionalidad) {
		driver.findElement(txtNacionalidad).sendKeys(strNacionalidad);
	}


	public void setTxtCarnetRes(String strCarnetRes) {
		driver.findElement(txtCarnetRes).sendKeys(strCarnetRes);
	}


	public void setTxtCarnetMinoridad(String strCarnetMinoridad) {
		driver.findElement(txtCarnetMinoridad).sendKeys(strCarnetMinoridad);
	}


	public void setTxtPartidaNacimiento(String strPartidaNacimiento) {
		driver.findElement(txtPartidaNacimiento).sendKeys(strPartidaNacimiento);
	}

	//Nacional
	@Description("Creacion rapida de cliente persona natural nacional")
	public void creacionRapidaPersona(String dui, String nombres, String primerApell, String segApell, String sexo, String estCiv,
			String profesion, String pais, String depto, String cuidad, String tipoCta) {
		this.clickClientes();
		this.clickPersonas();
		this.clickCreacionRapida();
		
		if(tipoCta.equals("CtaAho")) {
			this.clickAho();
		}else {
			this.clickCtaCte();
		}
	
		this.setTxtDui(dui);
		//ingreso nit
		this.setTxtNombres(nombres);
		this.setTxtPrimerApellido(primerApell);
		this.setTxtSegundoApellido(segApell);
		this.setTxtSexo(sexo);
		this.setTxtEstadoCivil(estCiv);
		//fecha de nacimiento
		this.setTxtProfesion(profesion);
		this.setTxtPais(pais);
		this.setTxtDepto(depto);
		this.setTxtCiudad(cuidad);
		
		this.clickTransmitir();
	}
	
	//extranjero
	@Description("Creacion rapida de cliente persona natural extranjero")
	public void creacionRapidaPersona(String nombres, String primerApell, String segApell, String sexo, String estCiv,
			String profesion, String pais, String depto, String cuidad, String tipoCta, String pasaporte, String nacionalidad, String carRes) {
		this.clickClientes();
		this.clickPersonas();
		this.clickCreacionRapida();
		
		this.clicExtranjero();
		
		if(tipoCta.equals("CtaAho")) {
			this.clickAho();
		}else {
			this.clickCtaCte();
		}
	
		
		//ingreso nit
		this.setTxtNombres(nombres);
		this.setTxtPrimerApellido(primerApell);
		this.setTxtSegundoApellido(segApell);
		this.setTxtSexo(sexo);
		this.setTxtEstadoCivil(estCiv);
		this.setTxtPasaporte(pasaporte);
		//fecha expedicion pasaporte
		//fecha vencimiento pasaporte
		this.setTxtNacionalidad(nacionalidad);
		this.setTxtCarnetRes(carRes);
		//fecha de nacimiento
		this.setTxtProfesion(profesion);
		this.setTxtPais(pais);
		this.setTxtDepto(depto);
		this.setTxtCiudad(cuidad);
		
		this.clickTransmitir();
	}
	
	//menor de edad
	@Description("Creacion rapida de cliente persona natural menor de edad")
	public void creacionRapidaPersona(String nombres, String primerApell, String segApell, String sexo, String estCiv,
			String profesion, String pais, String depto, String cuidad, String tipoCta) {
		this.clickClientes();
		this.clickPersonas();
		this.clickCreacionRapida();
		
		this.clicExtranjero();
		
		if(tipoCta.equals("CtaAho")) {
			this.clickAho();
		}else {
			this.clickCtaCte();
		}
	
		
		
		this.setTxtNombres(nombres);
		this.setTxtPrimerApellido(primerApell);
		this.setTxtSegundoApellido(segApell);
		this.setTxtSexo(sexo);
		this.setTxtEstadoCivil(estCiv);
		//fecha de nacimiento
		this.setTxtProfesion(profesion);
		this.setTxtPais(pais);
		this.setTxtDepto(depto);
		this.setTxtCiudad(cuidad);
		
		this.clickTransmitir();
		
		//click en aceptar cuadro de dialogo
	}
	
	
}
